package com.legacy.step;

import java.util.UUID;

import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.entity.ai.attributes.AttributeModifier;
import net.neoforged.bus.api.SubscribeEvent;
import net.neoforged.neoforge.common.NeoForgeMod;
import net.neoforged.neoforge.event.ItemAttributeModifierEvent;

public class StepEntityEvents
{
	private static final AttributeModifier STEPPING_MODIFIER = new AttributeModifier(UUID.fromString("7782cebb-c0fb-4eba-9473-28e41c7a9b18"), "Stepping Enchantment modifier", 1.0D, AttributeModifier.Operation.ADDITION);

	@SubscribeEvent
	public static void modifyItemAttrs(ItemAttributeModifierEvent event)
	{
		if (event.getSlotType().equals(EquipmentSlot.FEET))
		{
			float enchantmentLevel = event.getItemStack().getEnchantmentLevel(StepRegistry.STEPPING.get());

			if (enchantmentLevel > 0)
				event.addModifier(NeoForgeMod.STEP_HEIGHT.value(), STEPPING_MODIFIER);
		}
	}
}
