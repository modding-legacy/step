package com.legacy.step;

import net.minecraft.core.registries.Registries;
import net.minecraft.tags.TagKey;
import net.minecraft.world.item.enchantment.Enchantment;

public interface StepEnchantmentTags
{
	public static void init()
	{
	}

	TagKey<Enchantment> STEPPING_INCOMPATIBLE = tag("stepping_incompatible"); 

	private static TagKey<Enchantment> tag(String key)
	{
		return TagKey.create(Registries.ENCHANTMENT, StepMod.locate(key));
	}
}