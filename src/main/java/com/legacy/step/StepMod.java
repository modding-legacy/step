package com.legacy.step;

import net.minecraft.resources.ResourceLocation;
import net.neoforged.bus.api.IEventBus;
import net.neoforged.fml.common.Mod;
import net.neoforged.fml.event.lifecycle.FMLCommonSetupEvent;
import net.neoforged.neoforge.common.NeoForge;

@Mod(StepMod.MODID)
public class StepMod
{
	public static final String MODID = "step";

	public static ResourceLocation locate(String name)
	{
		return ResourceLocation.fromNamespaceAndPath(MODID, name);
	}

	public StepMod(IEventBus modBus)
	{
		NeoForge.EVENT_BUS.register(StepEntityEvents.class);
		modBus.addListener((FMLCommonSetupEvent event) -> StepEnchantmentTags.init());
	}
}