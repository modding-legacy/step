package com.legacy.step;

import net.minecraft.core.registries.BuiltInRegistries;
import net.minecraft.world.entity.EquipmentSlot;
import net.minecraft.world.item.enchantment.Enchantment;
import net.minecraft.world.item.enchantment.EnchantmentCategory;

public class SteppingEnchantment extends Enchantment
{
	public SteppingEnchantment(Enchantment.Rarity rarityIn, EquipmentSlot... slots)
	{
		super(rarityIn, EnchantmentCategory.ARMOR_FEET, slots);
	}

	@Override
	public int getMinCost(int enchantmentLevel)
	{
		return enchantmentLevel * 10;
	}

	@Override
	public int getMaxCost(int enchantmentLevel)
	{
		return this.getMinCost(enchantmentLevel) + 15;
	}

	@Override
	public int getMaxLevel()
	{
		return 1;
	}

	@Override
	public boolean checkCompatibility(Enchantment ench)
	{
		return super.checkCompatibility(ench) && !BuiltInRegistries.ENCHANTMENT.wrapAsHolder(ench).is(StepEnchantmentTags.STEPPING_INCOMPATIBLE);
	}
}