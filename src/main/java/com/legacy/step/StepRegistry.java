package com.legacy.step;

import net.minecraft.advancements.critereon.DamageSourcePredicate;
import net.minecraft.advancements.critereon.TagPredicate;
import net.minecraft.core.registries.Registries;
import net.minecraft.data.worldgen.BootstrapContext;
import net.minecraft.resources.ResourceKey;
import net.minecraft.tags.DamageTypeTags;
import net.minecraft.tags.EnchantmentTags;
import net.minecraft.tags.ItemTags;
import net.minecraft.world.entity.EquipmentSlotGroup;
import net.minecraft.world.item.enchantment.Enchantment;
import net.minecraft.world.item.enchantment.EnchantmentEffectComponents;
import net.minecraft.world.item.enchantment.LevelBasedValue;
import net.minecraft.world.item.enchantment.effects.AddValue;
import net.minecraft.world.level.storage.loot.predicates.DamageSourceCondition;

//@EventBusSubscriber(modid = StepMod.MODID, bus = Bus.MOD)
public class StepRegistry
{
	/*public static final Lazy<Enchantment> STEPPING = Lazy.of(() -> new SteppingEnchantment(Rarity.RARE, EquipmentSlot.FEET));
	
	@SubscribeEvent
	public static void onRegistry(RegisterEvent event)
	{
		if (event.getRegistryKey().equals(Registries.ENCHANTMENT))
			event.register(Registries.ENCHANTMENT, StepMod.locate("stepping"), STEPPING);
	
	}*/
	public static final ResourceKey<Enchantment> STEPPING = ResourceKey.create(Registries.ENCHANTMENT, StepMod.locate("stepping"));

	public static void enchantmentBootstrap(BootstrapContext<Enchantment> bootstrap)
	{
		var itemReg = bootstrap.lookup(Registries.ITEM);
		var enchReg = bootstrap.lookup(Registries.ENCHANTMENT);

		register(bootstrap, STEPPING, Enchantment.enchantment(Enchantment.definition(itemReg.getOrThrow(ItemTags.ARMOR_ENCHANTABLE), 10, 4, Enchantment.dynamicCost(1, 11), Enchantment.dynamicCost(12, 11), 1, EquipmentSlotGroup.FEET)).exclusiveWith(enchReg.getOrThrow(EnchantmentTags.ARMOR_EXCLUSIVE)).withEffect(EnchantmentEffectComponents.DAMAGE_PROTECTION, new AddValue(LevelBasedValue.perLevel(1.0F)), DamageSourceCondition.hasDamageSource(DamageSourcePredicate.Builder.damageType().tag(TagPredicate.isNot(DamageTypeTags.BYPASSES_INVULNERABILITY)))));
	}

	private static void register(BootstrapContext<Enchantment> pContext, ResourceKey<Enchantment> pKey, Enchantment.Builder pBuilder)
	{
		pContext.register(pKey, pBuilder.build(pKey.location()));
	}
}
